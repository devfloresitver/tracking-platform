<?php

use Illuminate\Support\Facades\Route;
use App\Order;
use App\User;
use App\Package;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
	$algo = User::all();
	$algo = Order::all();
	//dd($algo);
    return view('home')->with('algo',null);
});


Route::prefix('user')->group(function(){
	Route::get('/profile', 'UserController@index');
	Route::post('/update', 'UserController@updateDetails');
	Route::get('/envios', 'UserController@envios');
	Route::get('/facturas', 'UserController@facturas');
	Route::get('/movimientos', 'UserController@movimientos');
});


Route::prefix('package')->group(function(){
	Route::get('/new', 'PackageController@index');
	Route::post('/create', 'PackageController@create');
});

Route::prefix('checkout')->group(function(){
	Route::get('/buy', 'OrderController@checkout');
	Route::post('/buy', 'OrderController@success');
	Route::get('/err', 'OrderController@error');
});



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
