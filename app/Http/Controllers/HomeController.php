<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;


class HomeController extends Controller
{

    protected $newPackage;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    public function estimatePrice(Request $request){
        //$request->key('width');
        //$request->key('heigth');
        //$request->key('weigth');
        //2cmx2cm, 2kgs 
        return 2 * 2 * 0.2;
    }

    public function getCountries(){
        //consume api
    }

    public function getAddresses(){
        //comsune addresses
    }
}
