<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Address;
use App\Order;
use Hash;
class UserController extends Controller
{


    protected $pwdAttempts = 0;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $id = session('id') ? 1 : 0;
        $userId = auth()->id();
        $user = User::find(45);
        dd($user->getAuthPassword(), $user->password);
        //dd( $user->addresses()->get());
        return view('user/envios')
        ->with('user', $user);

    }

    public function updateDetails(Request $request){

        //dd(auth()->id());

        //$pwdActual = $request->get('uPassAct');
        $nuevoPwd = $request->get('uPass');
        $pwdConf = $request->get('uConfPass');
        $id = auth()->id();
        

        $pwdValido = $this->validaPassword($nuevoPwd, $pwdConf, $id);

        dd('ok');

        if($pwdValido){
        $user = User::find($id);
        $user->id = $id;//session('id');
        $user->name = $request->get('uNombre');
        $user->email = $request->get('uCorreo');
        $user->password = $request->get('uPass');
     
        //uCmbPred

        $direccion = $user->addresses()->get();
        $nDirecciones = $direccion->sizeof();
        dd($direccion[0]);
        $user_address = Address::find($direccion[0]->id);
        $user_address->address = $request->get('uAddress');
        $user_address->cp = $request->get('uCP');
        $user_address->city = $request->get('uCity');
        $user_address->references = $request->get('uReferences');

        $user_address->save();
        $user->save();
        $status = 1;
        }
        else{
            $status = 0;
        }

         return back()->with('status', $status);
    }


    protected function validaPassword($nuevo, $confimarcion, $id){

        $actual = User::find($id)->password;
        $actual = auth()->getAuthPassword();


        if($nuevo === $confimarcion && !Hash::check($nuevo, $actual)){
            $pwdAttempts++;
            session(['pwdAttempts', $pwdAttempts]);    
            return true;   
        }

        
    }


    public function envios(){
        dd(User::find(auth()->id()));
        $envios = Order::where('order_user', '=', 1)->get();
        //dd($envios);
        return view('user/envios')->with('envios', $envios);
    }

    public function facturas(){
        return view('user/facturas');
    }

    public function movimientos(){
        return view('user/movimientos');
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
