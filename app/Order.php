<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    public function users(){
    	return $this->belongsTo(User::class, 'order_user');
    }

    public function packages(){
    	return $this->hasOne(Package::class, 'package_order', 'id');
    }
}
