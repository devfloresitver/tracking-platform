<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->unsignedBigInteger('service_id');
            
            //$table->unsignedBigInteger('api_id');
            $table->integer('type');
            $table->string('origin');
            $table->text('destiny');
            $table->text('observations');
            $table->string('weight');
            $table->string('dimensions');
            $table->date('dispatch_date');
            $table->date('delivery_date');
            $table->string('received_by');
            $table->integer('try');
            $table->unsignedBigInteger('package_order');
            
            $table->foreign('package_order')->references('id')
            ->on('orders');
           

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
