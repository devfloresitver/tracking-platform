<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->float('total',8,2)->default(200.0);
            $table->unsignedBigInteger('method');
            $table->tinyInteger('order_status')->default(0);
            $table->tinyInteger('status')->default(0);
            $table->integer('type')->default(0);
            $table->unsignedBigInteger('order_user');
            $table->foreign('order_user')->references('id')
            ->on('users')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
