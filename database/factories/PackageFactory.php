<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Package;
use Faker\Generator as Faker;
use App\Order;

$factory->define(Package::class, function (Faker $faker) {
    return [
        'service_id' => $faker->randomNumber(1,  $strict = true),
        //'api_id' => $faker->randomNumber(3);
        'type' => rand(1,4),
        'origin' => $faker->country(),
        'destiny' => $faker->country(),
        'observations' => $faker->realText(50),
        'weight' => $faker->randomFloat(2, 5, 10),
        'dimensions' => $faker->realText(20),
        'dispatch_date' => $faker->date(),
        'delivery_date' => $faker->date(),
        'received_by' => $faker->dateTimeBetween($startDate='-1 year', $endDate = 'now'),
        'try' => rand(1,3),
        'package_order' =>rand(1, Order::all()->count())
        

    ];
});
