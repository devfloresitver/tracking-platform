<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Order;
use Faker\Generator as Faker;
use App\User;
$factory->define(Order::class, function (Faker $faker) {
    return [
        'total' => $faker->randomFloat(2, 100.0, 200.0),
        'method' => $faker->randomNumber(1),
        'order_status' => $faker->randomNumber(1),
        'status' => $faker->boolean(50),
        'type' => $faker->randomNumber(1),
        'order_user' => rand(1, User::all()->count())
    ];
});
