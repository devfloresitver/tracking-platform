<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Address;
use Faker\Generator as Faker;
use App\User;
$factory->define(Address::class, function (Faker $faker) {
    return [
    	'address' => $faker->address(),
    	'cp' => $faker->randomNumber(2,  $strict = true),
    	'city' => $faker->city(),
    	'reference' => $faker->country(),
    	'user_address' => rand(1, User::all()->count())
          
    ];
});
