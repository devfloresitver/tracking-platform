<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    return [
        'description' => $faker->realText(50),
        'api_id' => $faker->randomNumber(3)

    ];
});
