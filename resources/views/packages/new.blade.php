<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

@include('layouts/head')

<body>

    <div class="cotainer">

        @include('layouts/nav')

        <main>
            <hr>

            <div class="text-center bg-transparent text-black">

                <div id="address-detail" class="pb-4 pt-3 container  ">
                    <div class="row">
                        <div class="col">
                            <span>FLAG 91800 Veracruz</span> ->
                            <span>FLAG 91800 Veracruz</span>
                        </div>

                        <div class="col">

                        </div>
                    </div>

                </div>
            </div>
    </div>

    <div class="bs-stepper">
        <div class="bs-stepper-header" role="tablist">
            <!-- your steps here -->
            <div class="step" data-target="#service-part">
                <button type="button" class="step-trigger" role="tab" aria-controls="service-part" id="service-part-trigger">
                    <span class="bs-stepper-circle">1</span>
                    <span class="bs-stepper-label">Elegir un Servicio</span>
                </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#address-part">
                <button type="button" class="step-trigger" role="tab" aria-controls="address-part" id="address-part-trigger">
                    <span class="bs-stepper-circle">2</span>
                    <span class="bs-stepper-label">Agregar una dirección</span>
                </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#payment-part">
                <button type="button" class="step-trigger" role="tab" aria-controls="payment-part" id="payment-part-trigger">
                    <span class="bs-stepper-circle">3</span>
                    <span class="bs-stepper-label">Pago</span>
                </button>
            </div>
            <div class="line"></div>
            <div class="step" data-target="#guide-part">
                <button type="button" class="step-trigger" role="tab" aria-controls="guide-part" id="guide-part-trigger">
                    <span class="bs-stepper-circle">4</span>
                    <span class="bs-stepper-label">Imprimir guía</span>
                </button>
            </div>
        </div>
        <div class="bs-stepper-content">
            <!-- your steps content here -->
            <div id="service-part" class="content" role="tabpanel" aria-labelledby="service-part-trigger">
                <table class="table bg-white  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Servicio</th>
                            <th scope="col">Entrega estimada</th>
                            <th scope="col">Detalles</th>
                            <th scope="col">Precio</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">Redpack</th>
                            <td>DATE</td>
                            <td>TIPO</td>
                            <td>PRECIO INC.IVA
                                <button type="button" class="btn btn-primary envio">Enviar</button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">FDX</th>
                            <td>DATE</td>
                            <td>TIPO</td>
                            <td>PRECIO
                                <button type="button" class="btn btn-primary envio">Enviar</button>
                            </td>
                        </tr>
                        <tr>
                            <th scope="row">Estafeta</th>
                            <td>DATE</td>
                            <td>TIPO</td>
                            <td>PRECIO
                                <button type="button" class="btn btn-primary envio">Enviar</button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <div id="address-part" class="content" role="tabpanel" aria-labelledby="address-part-trigger">

                <div id="personal-address" class="pb-4 pt-3 container  ">
                    <div class="row">
                        <div class="col">
                            <div class="card">
                                <div class="card-header">
                                    Datos personales y Contenido del envío
                                </div>

                                <div class="card-body">
                                    <form id="frmServicios" action="" method="post">
                                        @csrf
                                        <div class="form-group">
                                            <label>Nombre(s)*</label>
                                            <input type="text" name="nombreServ" class="form-control" 
                                             required>

                                        </div>
                                        <div class="form-group">
                                            <label>Apellido(s)*</label>
                                            <input type="text" name="apellidosServ" class="form-control" required>

                                        </div>
                                        <div class="form-group">
                                            <label>Correo*</label>
                                            <input type="email" name="correoServ" class="form-control" required>

                                        </div>
                                        <div class="form-group">
                                            <label>Contenido de tu envío*</label>
                                            <input type="text" name="contenidoServ" class="form-control" required>

                                    </form>
                                </div>

                            </div>
                        </div> 
                         <div class="row pt-3">
                             <div class="col">
                              <div class="card">
                                <div class="card-header">
                                  Origen
                                </div>
                                <div class="card-body">
                                  <form id="frmOrigen" action="" method="post">
                                    @csrf
                                        <div class="form-group">
                                            <label>Nombre(s) y Apellido*</label>
                                            <input type="text" name="nombreOrg" class="form-control"required>

                                        </div>
                                        <div class="form-group">
                                            <label>Empresa</label>
                                            <input type="text" name="empresaOrg" class="form-control"
                                            required>

                                        </div>
                                        <div class="form-group">
                                            <label>Telefono*</label>
                                            <input type="tel" name="telefonoOrg" class="form-control"
                                            required>

                                        </div>
                                        <div class="form-group">
                                            <label>Correo electrónico*</label>
                                            <input type="mail" name="correoOrg" class="form-control"
                                            required>
                                        </div>

                                        <div class="form-group">
                                            <label>Calle y Número</label>
                                            <input type="text" name="calleOrg" class="form-control" required>

                                        </div>

                                        <div class="form-group">
                                            <label>Dirección Línea 2.</label>
                                            <input type="text" name="lineaOrg" class="form-control" required>

                                        </div>

                                        <div class="form-group">
                                            <label>Colonia</label>
                                            <input type="text" name="coloniaOrg" class="form-control"required>

                                        </div>

                                        <div class="form-group">
                                            <span>98000 Veracruz, VER.</span>

                                        </div>

                                    </form>
                                </div>
                               
                             </div>
                           </div>
                              <div class="col">
                              <div class="card">
                                <div class="card-header">
                                  DESTINO
                                </div>
                                <div class="card-body">
                                  <form id="frmDestino" action="" method="post">
                                    @csrf
                                        <div class="form-group">
                                            <label>Nombre(s) y Apellido*</label>
                                            <input type="text" name="nombreDest" class="form-control"required>

                                        </div>
                                        <div class="form-group">
                                            <label>Empresa</label>
                                            <input type="text" name="empresaDest" class="form-control"required>

                                        </div>
                                        <div class="form-group">
                                            <label>Telefono*</label>
                                            <input type="tel" name="telefonoDest" class="form-control"required>

                                        </div>
                                        <div class="form-group">
                                            <label>Correo electrónico*</label>
                                            <input type="mail" name="correoDest" class="form-control"required>
                                        </div>

                                        <div class="form-group">
                                            <label>Calle y Número</label>
                                            <input type="text" name="calleDest" class="form-control"required>

                                        </div>

                                        <div class="form-group">
                                            <label>Dirección Línea 2.</label>
                                            <input type="text" name="lineaDest" class="form-control"required>

                                        </div>

                                        <div class="form-group">
                                            <label>Colonia</label>
                                            <input type="text" name="coloniaDest" class="form-control"required>

                                        </div>

                                        <div class="form-group">
                                            <span>98000 Veracruz, VER.</span>

                                        </div>

                                    </form>
                                </div>
                               
                             </div>
                           </div>
                          </div> 
                       <div class="row pt-3">
                             <div class="col">
                              <div class="card">
                                <div class="card-header">
                                  Detalles del envío
                                </div>
                                <div class="card-body">
                                  <form id="frmEnvio" action="" method="">
                                        <div class="form-group">
                                            <label>Fecha del Envío*</label>
                                            <input type="text" name="fechaEnv" class="form-control"
                                            required>

                                        </div>
                                        <div class="form-group">
                                            <label>Tipo del envío*: </label>
                                              <label for="pack">
                                                Paquete
                                                <input type="radio" name="packEnv" class="form-control" checked>
                                              </label>
                                              
                                              
                                              <label for="doc">
                                              Documento  
                                              <input type="radio" name="docEnv" class="form-control">
                                              </label>
                                              
                                        </div>

                                        <div class="form-group">
                                            <label>Contenido</label>
                                             <input type="text" name="contenidoEnv" class="form-control">
                                        </div>

                                        <div class="form-group">
                                            <label>Referencia(s)</label>
                                             <input type="text" name="referenciaEnv" class="form-control">
                                        </div>

                                        <div class="form-group">

                                            <label for="seguro">
                                              <input type="checkbox" name="seguroEnv" class="form-control">
                                              Asegurar envío
                                            
                                            </label>
                                             
                                        </div>


                                    </form>
                                </div>
                               
                             </div>
                           </div>


                          </div> 
                      </div>
                            <div class="col-4">
                                <div class="container pb-5  text-center">
                                  <div class="card">
                                    <div class="card-header">
                                    <p class="lead">FDX</p>
                                  </div>

                                  <div class="card-body">
                                    <p>De:</p>
                                    <p>ASDSAD</p>
                                    <p>A:</p>
                                    <p>dadsa</p>
                                    <p>Peso: 0.55KG</p>
                                    <p>Precio Neto: MX$141.11</p>
                                    <p>IVA: MX$141.11</p>
                                    
                                  </div>

                                  <div class="card-footer">
                                   <p><strong>Total: MX$2232.23</strong></p>
                                  </div>   
                                  </div>
                                  
                                    
                                    
                                </div>
                            </div>


                   

                      
                    </div>

                </div>
            </div>

        </div>

        <div id="payment-part" class="content" role="tabpanel" aria-labelledby="address-part-trigger">

          <div class="pb-4 pt-3 container  ">
            <div class="row">
                      <div class="col-8">
            <div class="card">
            <div class="card-header">
              Pago
            </div>

            <div class="card-body">
            <form id="frmPago" action="" method="get" accept-charset="utf-8">
                
                
            
               <label for="paypal">
                Paypal
                <input type="radio" name="paypal" class="form-control" checked>
              </label>
              <br>
             <label for="paypal">
                Tárjeta de débito o crédito
                <input type="radio" name="card" class="form-control" checked>
              </label>
              <br>
             <label for="paypal">
                Oxxo
                <input type="radio" name="oxxo" class="form-control" checked>
              </label>
            </div>

            </form>
          </div>  
          </div>
          <div class="col-4">
            <div class="card">
              <div class="card-header">
                FDX
              </div>
              <div class="card-body">
                <p class="text">De:</p> <span>91848, Boca, Mexico.</span>
                <p class="text">A:</p> <span>9989, Ciudad de Mexico.</span>
                <p class="text">Peso</p> <span>02kg</span>
                <p class="text">Precio Neto: </span> MXN$343.3</span>
                <strong class="text">IVA(16%): </strong> <strong>MX$2322.2</strong>
                
              </div>
            </div>
          </div>
          </div>

          
          
          </div>



        </div>

        <div id="guide-part" class="content" role="tabpanel" aria-labelledby="guide-part-trigger">
        </div>

        <div class="container text-center pb-3">

            <button type="button" id="btnNext" class="btn btn-primary">Next</button>
            <button type="button" id="btnPrevious" class="btn btn-primary">Previous</button>

        </div>
    </div>
    </div>

    <script>
        var stepper = new Stepper(document.querySelector('.bs-stepper'));

        document.addEventListener('DOMContentLoaded', function() {
            new Stepper(document.querySelector('.bs-stepper'));
        });

        let btnNext = document.getElementById('btnNext');
        let btnPrevious = document.getElementById('btnPrevious');

        btnNext.addEventListener('click', () => {

            stepper.next();
        });

        btnPrevious.addEventListener('click', () => {
            stepper.previous();
        });
    </script>
    </main>

    @include('layouts/footer')
</body>

</html>