@extends('layouts.general') @section('content')

<div class="container pb-2">
    <h1>Detalles de Guia</h1>
</div>

<div class="container mt-2 bg-white round">
    <div class="row">
        <div class="col">

            <div id="divGuiaMain" class="container mt-3">

                <div class="row" id="setting-u">
                    <div class="col">
                        <span>GUIA</span>
                        <p class="text">
                            391008341166
                        </p>
                    </div>
                    <div class="col">
                        <span>PAQUETERIA</span>
                        <p class="text">
                            FEDEX
                        </p>
                    </div>
                    <div class="col">
                        <span>SERVICIO</span>
                        <p class="text">
                            Express
                        </p>
                    </div>
                    <div class="col">
                        <span>ESTATUS</span>
                        <p class="text">
                            ENTREGADO
                        </p>
                    </div>
                    <div class="col">
                        <span>COSTO TOTAL</span>
                        <p class="text">
                            187.00
                        </p>
                    </div>
                    <div class="col">
                        <span>CREACION</span>
                        <p class="text">
                            10/03/2020
                        </p>
                    </div>
                    <div class="col">
                      <form id="frmGuia" action="" method="get" accept-charset="utf-8">
                        {{--id will be retrieved by using session()--}}
                        <button type="button" class="btn btn-primary round shadow">
                            Descargar
                        </button>

                      </form>
                        
                    </div>
                </div>

            </div>




        </div>

    </div>

    <hr>

    <div class="row">
        <div class="col">

            <div id="divGuiaCont" class="container mt-3">

                <div class="row">
                    <div class="col">
                        <span>CONTENIDO</span>
                        <p class="text">
                            audifonos y smartband
                        </p>
                    </div>
                    <div class="col">
                        <span>PESO Y MEDIDAS PAGADO</span>
                        <p class="text">
                            1KB, 10CM,10CM, 12CM
                        </p>
                    </div>
                    <div class="col">
                        <span>PESO Y MEDIDAS USADO</span>
                        <p class="text">
                            Indefinido
                        </p>
                    </div>
                    <div class="col">
                        <span>PRECIO COTIZADO</span>
                        <p class="text">
                            $167.00 mxn
                        </p>
                    </div>
                    <div class="col">
                        <span>SOBREPESO Y MONTO</span>
                        <p class="text">
                            Indefinido
                        </p>
                    </div>
                  
                </div>

            </div>




        </div>

    </div>

</div>

<div class="container mt-2 bg-white round">
  <div class="row">
      <div class="col">
          <h2>ORIGEN</h2>
          <span>NOMBRE</span>
          <p>Juan Pablo Lopez</p>
                    <span>EMPRESA</span>
          <p>Smartchoice</p>
          <span>CALLE Y NUMERO</span>
          <p>BLVD. Miguel Almena #98</p>
          <span>CIUDAD ESTADO Y CP</span>
          <p>Bocal del Rio, Veracruz, 94290</p>
          <span>REFERENCIA</span>
          <p>Jacal 2l estafeta</p>


      </div>
      <div class="col">
          <h2>DESTINO</h2>
          
          <span>NOMBRE</span>
          <p>Francisco Andrade</p>
          <span>EMPRESA</span>
          <p>NA Andrade</p>

          <span>CALLE Y NUMERO</span>
          <p>Unidad Natitivas Xochimilco 39</p>

          <span>COLONIA</span>
          <p>Tablas de San Lorenzo</p>

          <span>CIUDAD, ESTADO C.P.</span>
          <p>Xochimilco, CDMX, 16090</p>
          
          <span>REFERENCIA</span>
          <p>M2 ENT. C DEPTO 202</p>


      </div>
  </div>
</div>

<div class="container mt-2">
  <h1>Historial de envío</h1>
  <div class="row">
     <table class="table bg-white  text-center table-borderless">
                    <thead>
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">Hora</th>
                            <th scope="col">Descripcion</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">13/03/2020</th>
                            <td>11:23:00</td>
                            <td>Delivered</td>
                            {{-- delivered,
                              in transit,
                              on fedex vehicle for delivery,
                              in transit,
                              at local fedex facility,
                              --}}
                            
                        </tr>
                    </tbody>
                </table>
  </div>
</div>





@endsection