<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
 
 @include('layouts/head')
 
    <body>

        <div class="cotainer">

@include('layouts/nav')

    <main>
       <hr>
        <div >
           
          <div class="alert alert-primary" role="alert">
          <h1>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</h1>
          tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
          quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
          consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
          cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
          proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
       
        </div>    
        </div>
        <div class="text-center bg-dark text-white">
            <div id="cotizador" class="pb-4 pt-3 container  ">
            <div class="row">
                <div class="col">
                   <h1>COTIZADOR DE ENVÍOS</h1> 
                </div>
            </div>
            <div class="row">
                <div class="col">
                  <form action="#" method="post" accept-charset="utf-8">
                    @csrf
                    <p>Origen</p>

                    <input type="text" name="org" required>
                </div>
                <div class="col">
                      <p>Destino</p>
                    <input type="text" name="dest" required>
                </div>
                <div class="col">
                    <p>Envío un</p>
                    <label><input type="checkbox" name="doc" required>Documento</label>
                    <label><input type="checkbox" name="pack" required>Paquete</label>
                     <button type="submit" class="btn btn-primary">Cotizar Envío</button>
                </div>

                </form>
            </div>
            </div>

        </div>
        
    </main>
</div>

@include('layouts/footer')
    </body>
</html>
