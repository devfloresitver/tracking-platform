@extends('layouts.general')

@section('content')

<div class="container pb-2">
    <h1>Consulta de Movimientos</h1>
</div>

<div class="container">
       <form id="frmMov" action="" method="get" accept-charset="utf-8">
    <div class="row">
     
            
            
        
      <div class="col-6">
            <label for="fchInicio">Fecha de Inicio
        <input type="date" name="fchInicio" required>
        </label>

        <label for="fchFinal"class="ml-2">Fecha final
        <input type="date" name="fchFinal" required>
        </label>

      </div>
        <div class="col-3">
            <span>Tipo de Movimiento</span>
        <select class="custom-select" id="cmbMovimiento" required>
            <option selected>Choose...</option>
            <option value="1">One</option>
            <option value="2">Two</option>
            <option value="3">Three</option>
        </select>
        </div>
        </form>
    </div>
</div>

<div class="container mt-2">
    <table class="table bg-white  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">Tipo de Mov.</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Saldo Límite</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">13/03/2020 20:23:22PM</th>
                            <td>Creacion de Guia</td>
                            <td>$-107.22MXN</td>
                            <td>$03.00MXN</td>
                            
                        </tr>
                    </tbody>
                </table>
</div>

@endsection
