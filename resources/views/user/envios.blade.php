@extends('layouts.general')

@section('content')

<div class="container pb-2">
    <h1>Consulta de Envios</h1>
</div>

<div class="container">
     <form id="frmEnvios" action="" method="get" accept-charset="utf-8">
    <div class="row">
       
            
            
        
        <div class="col-3">
           <label for="buscaGuia">
            Busca por numero de guia o palabra clave
               <input type="text" name="buscaGuia" 
               class="form-control" required>
           </label>
           
        </div>

      <div class="col-5 text-center">
                <label for="fchInicio">Fecha de Inicio
                <input type="date" name="fchInicio" 
                class="form-control" required>
                </label>

            <label for="fchFinal"class="ml-2">Fecha final
            <input type="date" name="fchFinal"
             class="form-control" required>
            </label>

      </div>

        <div class="col-2">
            <span>Paquetería</span>
            <select class="custom-select" id="cmbMovimiento" required>
                <option selected>Choose...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        <div class="col-2">
            <span>Rastreo</span>
            <select class="custom-select" id="cmbMovimiento" required>
                <option selected>Choose...</option>
                <option value="1">One</option>
                <option value="2">Two</option>
                <option value="3">Three</option>
            </select>
        </div>

        </form>
    </div>
</div>

<div class="container mt-2">
    <table class="table bg-white  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">KG. Finales</th>
                            <th scope="col">Núm de Guía</th>
                            <th scope="col">Destinatario</th>
                            <th scope="col">Paqueteria</th>
                            <th scope="col">Tiempo estimado</th>
                            <th scope="col">
                            Rastreo</th>
                            <th scope="col">
                            Costo</th>
                            <th scope="col">Descargar</th>
                            
                        </tr>
                    </thead>
                    <tbody>
                        
                    @foreach($envios as $envio)
                           <tr>
                            <th scope="row">
                            {{$envio->created_at}}</th>
                            <td>
                            1</td>
                            <td>
                                <a id="env-u" href="/envio/" title="">{{$envio->id}}</a>
                            </td>
                            <td>Francisco Andrade
                            </td>
                            <td>
                                {{$envio->method}}
                            </td>
                            <td>
                                1-2 Dias hábiles*
                            </td>
                            <td>{{$envio->order_status ? 'No enviado' : 'Enviado'}}</td>
                            <td>{{$envio->total}}</td>
                            <td>
                                <button id="btnDescarga" class="btn btn-primary btn-block round">
                                    down
                                </button>
                            </td>
                        </tr>
                    @endforeach
                     
                    </tbody>
                </table>
</div>

@endsection
