@extends('layouts.general')

@section('content')


<div class="container">
    
</div>

<div class="container pb-2">
    <h1>Datos de Acceso</h1>
</div>

<div class="container mt-2">
    <div class="row">
        <div class="col">

            <div class="container">
                <h2>Datos de acceso</h2>
            </div>

            <form id="frmAjustes" action="{{url('user/update')}}" method="post" accept-charset="utf-8">
            @csrf
            
            <div class="col">
                        <label for="uNombre">
                        Nombre
                       <input type="text" name="uNombre" 
                       class="form-control" readonly required
                       value="{{$user->name}}">
                        </label>
                   <label for="uPass">
                    Nueva Contraseña
                       <input type="password" name="uPass" 
                       class="form-control" required
                       >
                   </label>
            </div>
                        <div class="col">
                      {{--   <label for="uEmpresa">
                        Nombre de la empresa
                       <input type="text" name="uEmpresa" 
                       class="form-control" readonly required>
                        </label> --}}
                   <label for="uConfPass">
                   Confirmar contraseña
                       <input type="password" name="uConfPass" 
                       class="form-control" required
                       >
                   </label>
            </div>
                        <div class="col">
                        <label for="uCorreo">
                       Correo
                       <input type="text" name="uCorreo" 
                       class="form-control" readonly required
                       value="{{$user->email}}">
                        </label>
{{--                    <label for="uPassAct">
                   Contraseña actual
                       <input type="text" name="uPassAct" 
                       class="form-control" required>
                   </label> --}}
            </div>

            <div class="row container ml-1 pb-4">
                <button id="btnGuardarAjustes" type="submit" 
                class="btn btn-primary round">Guardar Cambios</button>
            </div>


        </div>
        <div class="col">
            <div class="container">
                <h3>Dirección de origen predeterminada</h3>
            </div>



            <select class="custom-select" name="cmbDir" disabled>
                <option value="{{$user->addresses()->get()[0]->id}}">
                  {{$user->addresses()->get()[0]->address}}</option>
                
            </select>




            <div class="col">

            <label for="uAddress">
            Direccion
           <input type="text" name="uAddress" 
           class="form-control" required
           value="{{$user->addresses()->get()[0]->address}}">
            </label>

            <label for="uReferences">
            Referencia
           <input type="text" name="uReferences" 
           class="form-control" required
           value="{{$user->addresses()->get()[0]->reference}}">
            </label>

            <label for="uCity">
            Ciudad
           <input type="text" name="uCity"
            class="form-control" required
            value="{{$user->addresses()->get()[0]->city}}">
            </label>




            <label for="uCP">
            Código Postal
           <input type="text" name="uCP"
            class="form-control" required
            value="{{$user->addresses()->get()[0]->cp}}">
            </label>



{{-- 
                        <label for="uDirAlias">
                        Alias de dirección
                       <input type="text" name="uDirAlias" 
                       class="form-control" required>
                        </label>
                   <label for="uEmpO">
                   Empresa (opcional)
                       <input type="text" name="uEmpO"
                        class="form-control" required>
                   </label>
                <label for="uNombCont">
                        Nombre de Contacto
                       <input type="text" name="uNombCont" 
                       class="form-control" required>
                        </label>
                   <label for="uColonia">
                   Colonia
                       <input type="text" name="uColonia"
                        class="form-control" required>
                   </label>


                    <label for="uCalle">
                        Calle y número
                       <input type="text" name="uCalle" 
                       class="form-control" required>
                        </label>
                   <label for="uRefs">
                   Rerefencia (opcional)
                       <input type="text" name="uRefs"
                        class="form-control" required>
                   </label>


                    <label for="uCP">
                        Código Postal
                       <input type="text" name="uCP"
                        class="form-control" required>
                        </label>
                   <label for="uTel">
                   Teléfono
                       <input type="text" name="uTel" 
                       class="form-control" required>
                   </label> --}}

                <div class="row">
                   

                    <label for="uCmbPred">
                         <input type="checkbox" aria-label="Checkbox for following text input"
                    name="uCmbPred" required>
                        Establecer como mi dirección predeterminada.
                    </label>
                    

                </div>
                </form>
            </div>

        </div>
    </div>

     <div class="container">
      <hr>
        @if(session('status') === 1)
        <div class="alert alert-success mt-3" role="alert">
          Datos actualizados correctamente.
      </div>
        @elseif(session('status') === 0)
        <div class="alert alert-danger mt-3" role="alert">
          Error al actualizar datos.
      </div>
      @else

        @endif
     </div>
</div>

<script>

  
</script>

@endsection
