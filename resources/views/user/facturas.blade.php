@extends('layouts.general')

@section('content')

<div class="container">
    <h1>Facturas</h1>

    <form id="frmProfile" action="" method="get" accept-charset="utf-8">
        
        
    
    <div class="row">
        <div class="col-4">

            <span><strong>Buscar por:</strong></span>
           
                

           
            <input type="text" name="buscaFact" 
            class="form-control" placeholder="factura" required>
            <input type="text" name="cuenta" 
            class="form-control" placeholder="cuenta" required>

        </div>
        <div class="col-2">

            <strong>Por:</strong>
            <input type="date" name="desde" 
            required placeholder="" class="form-control">
            <input type="date" name="hasta" 
            required placeholder="" class="form-control">

        </div>
    </div>
    <div class="row">
        <div class="container text-left">
            <button type="submit" class="btn btn-primary pt-2 mt-2 mb-2">Buscar</button>
            <span class="link ml-1 text-muted text-decoration-underline">
            Limpiar búsqueda</span>
        </div>
    </div>
    </form>
</div>

<div class="container">
     <table class="table bg-white  text-center">
                    <thead>
                        <tr>
                            <th scope="col">Fecha</th>
                            <th scope="col">Factura</th>
                            <th scope="col">Monto</th>
                            <th scope="col">Fecha Límite</th>
                            <th scope="col">Orden</th>
                            <th scope="col">Acciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">13/03/2020</th>
                            <td>EnviaYA_FA61618468</td>
                            <td>MX$555.00</td>
                            <td>27/03/20</td>
                            <td>432468 envio</td>
                            <td>
                                <button type="button" class="btn btn-block btn-primary" >
                                    Descargar Factura
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
</div>

@endsection
